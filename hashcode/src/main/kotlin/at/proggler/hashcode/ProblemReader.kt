package at.proggler.hashcode

import java.util.*

class ProblemReader {
    fun read(){
        println(Scanner(ClassLoader.getSystemResourceAsStream("in.txt")!!).next())
    }
}
